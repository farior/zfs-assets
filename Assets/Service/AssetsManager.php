<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 03.03.15
 * Time: 17:58
 */

namespace ZFS\Assets\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AssetsManager implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * Function looking for asset directories in modules, located in paths,
     * defined in modulesDirectoryPath config key
     *
     * @return array
     */
    protected function getExistAssetsPaths()
    {
        $config = $this->getServiceLocator()->get('Config');
        $modulePaths = $config['ZFS\Assets']['modulesDirectoryPath'];
        $assetsDirectoryName = $config['ZFS\Assets']['assetsDirectoryName'];

        if (!is_array($modulePaths)) {
            $modulePaths = array($modulePaths);
        }

        $existAssets = array();

        foreach ($modulePaths as $modulePath) {
            $path = getcwd() . DIRECTORY_SEPARATOR . $modulePath;

            if (!file_exists($path) || !is_dir($path)) {
                continue;
            }

            $filesDirty = array_diff(scandir($path), array('.', '..'));

            foreach ($filesDirty as $dir) {
                $moduleDir = $path . DIRECTORY_SEPARATOR . $dir;

                if (!is_dir($moduleDir)) {
                    continue;
                }

                if (isset($config['ZFS\Assets'][$dir])) {
                    $assetDir = $moduleDir . DIRECTORY_SEPARATOR . $config['ZFS\Assets'][$dir];
                } else {
                    $assetDir = $moduleDir . DIRECTORY_SEPARATOR . $assetsDirectoryName;
                }

                if (file_exists($assetDir) && is_dir($assetDir)) {
                    $existAssets[$dir] = $assetDir;
                }
            }
        }

        return $existAssets;
    }

    /**
     * Create symbolic links to asset directories in public path
     *
     * @return array Result of symlink operations
     */
    public function createSymlinks()
    {
        $existAssetPaths = $this->getExistAssetsPaths();

        $publicDir = getcwd() . DIRECTORY_SEPARATOR . 'public';

        $linksCreated = array();

        foreach ($existAssetPaths as $moduleName => $assetPath) {
            $symlinkPath = $publicDir . DIRECTORY_SEPARATOR . $moduleName;

            if (is_link($symlinkPath)) {
                if (readlink($symlinkPath) !== $assetPath) {
                    unlink($symlinkPath);
                } else {
                    continue;
                }
            }

            $linksCreated[$symlinkPath] = symlink($assetPath, $symlinkPath);
        }

        return $linksCreated;
    }
}
