<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 03.03.15
 * Time: 16:58
 */

namespace ZFS\Assets\Controller;

use Zend\Console\ColorInterface as Color;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class AssetsController extends AbstractActionController
{
    /** @var $manager \ZFS\Assets\Service\AssetsManager */
    protected $manager;

    /** @var $console \Zend\Console\Adapter\Posix */
    protected $console;

    protected $config;

    public function onDispatch(MvcEvent $e)
    {
        $this->console = $this->getServiceLocator()->get('console');
        $this->config  = $this->getServiceLocator()->get('Config');
        $this->manager = $this->getServiceLocator()->get('AssetsManager');

        return parent::onDispatch($e);
    }

    public function symlinkAssetsAction()
    {
        $linksCreated = $this->manager->createSymlinks();

        foreach ($linksCreated as $link => $status) {
            $this->console->writeLine($link, $status ? Color::GREEN : Color::RED);
        }

        if (!empty($linksCreated)) {
            $this->console->writeLine(count($linksCreated).' symlink created', Color::YELLOW);
        }
    }
}
