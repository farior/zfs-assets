<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 13:10
 */

return array(
    'ZFS\Assets' => array(
        // Path to modules
        'modulesDirectoryPath' => array(
            'module',
            'vendor/zfstarter'
        ),
        // Assets directory name
        'assetsDirectoryName' => 'assets'
    ),
    'controllers' => array(
        'invokables' => array(
            'ZFS\Assets\Controller\Assets' => 'ZFS\Assets\Controller\AssetsController'
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'asset_symlink' => array(
                    'options' => array(
                        'route' => 'assets (-symlink|-s)',
                        'defaults' => array(
                            'controller' => 'ZFS\Assets\Controller\Assets',
                            'action' => 'symlinkAssets'
                        )
                    )
                )
            )
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            'AssetsManager' => 'ZFS\Assets\Service\AssetsManager',
        )
    ),
);
