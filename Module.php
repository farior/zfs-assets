<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 03.03.15
 * Time: 17:07
 */

namespace ZFS\Assets;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Console\Adapter\AdapterInterface;

class Module implements ConfigProviderInterface, ConsoleUsageProviderInterface
{
    public function getConfig()
    {
        return include  __DIR__ . '/config/module.config.php';
    }

    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'Symlink',
            'assets -s' => 'Create symlinks to modules assets in public directory',
        );
    }
}
